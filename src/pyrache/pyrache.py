"""
Main class for building pyrache package
"""
import logging
from typing import Any, Union

from redis import Redis, StrictRedis

from cache_classes import PickleSerializer, JsonSerializer, PyarrowSerializer
from pyrache.cache_classes.base_cache_class import BaseSerializer
from utils import hashing_key

log = logging.getLogger(__name__)

class PyRache():
    """
    Class to be initialized to allow caching
    """
    def __init__(self, redis_connection: Union[Redis, StrictRedis], leading_key: str = 'pyrache') -> None:
        """
        Class wrapper around Redis object to allow decorator wrapping

        Args:
            redis_connection (Union[Redis, StrictRedis]): Redis Connection object used to interact with the database
            leading_key (str, optional): [description]. Defaults to 'pyrache'.
        
        Raises:
            ValueError: [description]
            ConnectionError: [description]
        
        """
        # Checks the inputs for the object
        if isinstance(redis_connection, (Redis, StrictRedis)) is False:
            raise ValueError('redis_connection was not of correct type for interacting with redis database')        
        if isinstance(redis_connection, Redis):
            if redis_connection.ping() is False:
                raise ConnectionError('Could not connect to the redis connection object')


        # Sets attributes
        self.redis_connection = redis_connection
        self.leading_key = leading_key

        self.serializers = {}
        self.add_serializer(PickleSerializer())
        self.add_serializer(JsonSerializer())
        self.add_serializer(PyarrowSerializer())


    def add_serializer(self, serializer: BaseSerializer) -> None:
        """
        Add serialized to listing of methods

        Args:
            serializer (BaseSerializer): [description]
        """

        name = serializer.name
        self.serializers[name] = serializer    

    def key_generator(self, func, *args, **kwargs) -> str:
        """
        Takes function name and params and generates unique key with params hashed

        Args:
            func ([type]): [description]

        Returns:
            str: [description]
        """

        leading = f"{self.leading_key}.{func.__name__}"
        params = []

        if len(args) > 0:
            args_given = [str(i) for i in args]

            # Handling for self in casting class methods
            if args_given[0] == str(self.leading_key):
                args_given[0] = 'self'

            args_given = ', '.join(args_given)
            params.append(args_given)

        # Cleaning up kwargs
        if len(kwargs) > 0:
            for i in kwargs.keys():
                params.append("{}={}".format(i, kwargs[i]))

        # Hashes key with sha256 to generate a unique id
        hashed_params = hashing_key(params)
        
        # Combines the leading keys and the params together
        key = f"{leading}({hashed_params})"

        return key


    def deserialize(self, key: str, method: str='pickle') -> None:
        """[summary]

        Args:
            key (str): [description]
            method (str, optional): [description]. Defaults to 'pickle'.

        Raises:
            ValueError: [description]

        Returns:
            [type]: [description]
        """        
        
        pull_value = self.redis_connection.hget(key, 'hashed_value')
        method = self.redis_connection.hget(key, 'serialization_type')

        serializer = self.serializers.get(method, None)
        if serializer is None:
            raise ValueError('Serialization method was not one of "pickle, pyarrow, json"')

        serializer = serializer()
        value = serializer.deserialize(pull_value)

        return value


    def serialize(self, key: str, value: Any, method: str='pickle', timeout: int=60):
        """
        [summary]

        Args:
            key (str): [description]
            value (Any): [description]
            method (str, optional): [description]. Defaults to 'pickle'.
            timeout (int, optional): [description]. Defaults to 60.

        Raises:
            TypeError: [description]

        Returns:
            [type]: [description]
        """        
        # handles the serialization
        
        
        serializer = self.serializers.get(method, None)
        if serializer is None:
            raise ValueError('Serialization method was not one of "pickle, pyarrow, json"')

        hashed_value = serializer.serialize(value)

        # handles the posting to the redis
        self.redis_connection.hmset(key, {'hashed_value': hashed_value, 'serialization_type': method})

        if isinstance(timeout, int) is False and timeout is not None:
            raise TypeError('timeout was not a "int", instead recieved {}'.format(type(timeout)))

        if isinstance(timeout, int) is True:
            self.redis_connection.expire(key, timeout)

        return None


    def cache(self, force_key: str=None, method: str='pickle', timeout: int=60) -> Any:
        """
        For Decoratior structure look at
        url: https://stackoverflow.com/questions/5929107/decorators-with-parameters
        

        Args:
            force_key (str, optional): [description]. Defaults to None.
            method (str, optional): [description]. Defaults to 'pickle'.
            timeout (int, optional): [description]. Defaults to 60.

        Returns:
            Any: [description]
        """        
        
        def _decorator(func):
            @functools.wraps(func)
            def wrapper_df_decorator(*args, **kwargs):
    
                # generate key based on called function
                if force_key is None:
                    key = self.key_generator(func, *args, **kwargs)
                else:
                    key = self.key_generator(func, force_key)

                # check if exists in redis
                if self.redis_connection.exists(key) == 1:
                    # Pulls data from redis, deserialses and returns the dataframe
                    value = self.deserialize(key)
                else:
                    # Runs the function that was decorated
                    value = func(*args, **kwargs)

                    # Don't pickle none objects
                    if value is None:
                        return value

                    self.serialize(key, value, method, timeout)
                return value
            return wrapper_df_decorator
        return _decorator(force_key, method, timeout) if callable(force_key) else _decorator 