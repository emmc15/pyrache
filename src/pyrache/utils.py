import hashlib
from typing import Any, Iterable, List


def flatten_args_to_str(*args) -> List[Any]:
    """
    flatterns iterable args to a single flat list


    Returns:
        List[Any]: flat list of objects given
    """    
    # flattens args in case of a list is in the value
    values = []
    for i in args:
        if isinstance(i, Iterable):
            list_values = []
            for j in i:
                # Checks if value in list is none
                if j is not None:
                    # Adds to cleaned list values
                    list_values.append(str(j))
            # Sort inner list before adding to params values
            if len(list_values) > 0:
                list_values = sorted(list_values)
                for x in list_values:
                    values.append(x)
        else:
            values.append(str(i))
    return values


def hashing_key(*args) -> str:
    """
    Creates a hash of all the object given to create unique id
    using the sha256 algorith
    

    Returns:
        str: sha256
    """    
    values = flatten_args_to_str(*args)
    #Creates a key for redis cache
    # URL: https://www.pythoncentral.io/hashing-strings-with-python/
    key = ', '.join(values)
    generated_key = hashlib.sha256(key.encode())
    generated_key = generated_key.hexdigest()
    return generated_key

