"""
    Module used for serializing and desiralizing objects using pyarrow
"""
from typing import Any
import pyarrow

from base_cache_class import BaseSerializer


class PyarrowSerializer(BaseSerializer):
    """
    Object used for storing pyarrow object in redis
    """
    def __init__(self) -> None:
        name = 'pyarrow'  # pylint: disable=unused-variable
        self.context = pyarrow.default_serialization_context()


    def serialize(self, value: Any) -> bytes:
        """
        Method used to serialize an object via pyarrow

        Args:
            value (Any): An object that be dumped in pyarrow bytes format

        Returns:
            bytes: returns python bytes object
        """
        return self.context.serialize(value).to_buffer().to_pybytes()


    def deserialize(self, value: bytes) -> Any:
        """
        Returns a decoded value of the pyarrow bytes object

        Args:
            value (str): pyarrow bytes object

        Returns:
            Any: Deserilzed object
        """
        return self.context.deserialize(value)

