"""
    Module used for serializing and desiralizing objects using json
"""
from json import dumps, loads
from typing import Any

from base_cache_class import BaseSerializer


class JsonSerializer(BaseSerializer):
    """
    Object used for storing object in redis
    """
    def __init__(self) -> None:
        name = 'json'  # pylint: disable=unused-variable


    def serialize(self, value: Any) -> str:
        """
        Method used to serialize an object via json.dumps

        Args:
            value (Any): An object that be dumped in json format

        Returns:
            str: json.dumps returns jsonable str
        """
        return dumps(value)


    def deserialize(self, value: bytes) -> Any:
        """
        Returns a decoded value of the json str

        Args:
            value (str): jsonified str object

        Returns:
            Any: Dejsonified object
        """
        return loads(value)
