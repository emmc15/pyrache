"""
    Module used for serializing and desiralizing objects using pickle
"""
from typing import Any
from pickle import dumps, loads

from base_cache_class import BaseSerializer


class PickleSerializer(BaseSerializer):
    """
    Object used for storing pickle object in redis
    """
    def __init__(self) -> None:
        name = 'pickle'  # pylint: disable=unused-variable


    def serialize(self, value: Any) -> bytes:
        """
        Method used to serialize an object via pickle

        Args:
            value (Any): An object that be dumped in pickle bytes format

        Returns:
            bytes: returns python bytes object
        """
        return dumps(value)


    def deserialize(self, value: bytes) -> Any:
        """
        Returns a decoded value of the pickle bytes object

        Args:
            value (str): pickle bytes object

        Returns:
            Any: Deserilzed object
        """
        return loads(value)

