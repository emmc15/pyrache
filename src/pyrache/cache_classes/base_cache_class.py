"""
Base classe for PyRache Serializiation objects
"""
from abc import ABC, abstractmethod

class BaseSerializer(ABC):
    """
    Base class for creating serializtion objects

    Args:
        ABC (abc): Abstract base class
    """
    @property
    @abstractmethod
    def name(self):
        """
        Attribute name of the serialization type for quick access either in cache method
        """
        pass  # pylint: disable=unnecessary-pass

    @abstractmethod
    def serialize(self, value):
        """
        Method for storing object in redis
        """
        pass # pylint: disable=unnecessary-pass

    @abstractmethod
    def deserialize(self, value):
        """
        Method for deserializing object from redis
        """
        pass # pylint: disable=unnecessary-pass
