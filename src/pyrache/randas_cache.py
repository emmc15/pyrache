import json
import hashlib

# Imported Modules
import redis
import logging
import pickle

class RandasCache():

    def __init__(self, redis_instance=None, leading_key=None, hash_keys=False, disable_caching=False):

        if redis_instance is None:
            try:
                redis_instance = redis.Redis(host='localhost', port=6379, db=0)
                redis_instance.ping()
            except:
                logging.warning('Could not connect to local redis instance, caching will be disabled')
                redis_instance = None


        # Checks that a redis object has been passed
        if isinstance(redis_instance, redis.client.Redis) is not True and redis_instance is not None:
            raise AttributeError(
                "Did not recieve an Redis Object, instead recieved {}".format(type(redis_instance)))

        # Handles edge case of default redis connection not working
        if redis_instance is None:
            self.disable_caching = True
        else:
            self.disable_caching = disable_caching

        # Sets the redis object as an attribute of this class
        self.cache_container = redis_instance

        # pyarrow serializer, uses the default serialization method
        # https://stackoverflow.com/questions/57949871/how-to-set-get-pandas-dataframes-into-redis-using-pyarrow/57986261#57986261
        #self.context = pa.default_serialization_context()

        if leading_key is None:
            leading_key = 'RedisCache'

        self.leading_key = leading_key

        # leading for reference in db, can be used to identify the caching object in redis
        if isinstance(leading_key, str) is False:
            raise AttributeError('"Leading Key is intended to a str, instead recieved {}"'.format(leading_key))


        assert isinstance(hash_keys, bool), "[ERROR]: hash_keys should be a bool"
        self.hash_keys = hash_keys

        # storing of keys in the object so as to know how to retrieve the value
        self.keys = {}

    #------------------------------------------------------------------------------------
    # Internal Methods for mananging posting and getting from redis
    #------------------------------------------------------------------------------------
    @staticmethod
    def _flatten_args_to_str(*args):
        # flattens args in case of a list is in the value
        values = []
        for i in args:

            if isinstance(i, list):
                list_values = []
                for j in i:


                    # Checks if value in list is none
                    if j is not None:
                        # Adds to cleaned list values
                        list_values.append(str(j))

                # Sort inner list before adding to params values
                if len(list_values) > 0:
                    list_values = sorted(list_values)
                    for x in list_values:
                        values.append(x)
            else:
                values.append(str(i))

        return values

    @staticmethod
    def _hashing_key(*args):
        """
        Creates a hash of the object given to create unique id
        using the sha256 algorith
        Parameters:
        -----------
            args=must be able represented as a str
        Returns:
        --------
            str(), hash of the inputs
        """
        values = RedisCache._flatten_args_to_str(*args)
        #Creates a key for redis cache
        # URL: https://www.pythoncentral.io/hashing-strings-with-python/
        key = ', '.join(values)
        generated_key = hashlib.sha256(key.encode())
        generated_key = generated_key.hexdigest()
        return generated_key


    def key_generator(self, func, *args, **kwargs):
        """
        Generates a key for redis to cache based on the function
        it will decorate as well as the inputs to that function
        Returns:
            str()
        """
        leading = "{}.{}(".format(self.leading_key, func.__name__)

        params = []
        # Cleaning up args
        if len(args) > 0:
            args_given = [str(i) for i in args]

            # Handling for self in casting class methods
            if args_given[0] == str(self.leading_key):
                args_given[0] = 'self'

            args_given = ', '.join(args_given)
            params.append(args_given)

        # Cleaning up kwargs
        if len(kwargs) > 0:
            for i in kwargs.keys():
                params.append("{}={}".format(i, kwargs[i]))

        # Hashes key with sha256 to generate a unique id
        if self.hash_keys is True:
            # Takes list of params and joins into unique id string
            params = RedisCache._hashing_key(params)
            key = [leading, params]
        # Inserts the leading key and func string as prefix
        else:
            key = params
            key.insert(0, leading)
            #key = [leading, *params]
            #print(expand_params)


        # Creates the params section of the key
        str_params = ",".join(key[1:])
        # Combines the leading keys and the params together
        key = "{}{})".format(key[0], str_params)

        return key

    def _deserialize(self, key, method=None):
        """
        Method for deserializing python object
        Parameters:
        -----------
            key=value to use in the redis cache
        Returns:
        --------
            python object
        """
        pull_value = self.cache_container.hget(key, 'hashed_value')
        method = self.cache_container.hget(key, 'serialization')

        if method == 'pickle':
            value = pickle.loads(pull_value)
        elif method == 'pyarrow':
            value = pickle.loads(pull_value)
            #value = self.context.deserialize(pull_value)
        elif method == 'json':
            value = json.loads(pull_value)

        else:
            raise ValueError('Serialization method was not one of "pickle, pyarrow, json"')

        return value


    def _serialize(self, key, value, method='pickle', timeout=None):
        """
        Method for serializing python object
        Parameters:
        -----------
            key=value to use in the redis cache
            method=str(pickle or pyarrow), method to deserialize the object
        Returns:
        --------
            python object
        """
        # handles the serialization
        if method == 'pickle':
            hashed_value = pickle.dumps(value)
            self.keys[key] = 'pickle'

        elif method == 'pyarrow':
            hashed_value = pickle.dumps(value)
            self.keys[key] = 'pickle'

            #hashed_value = self.context.serialize(value).to_buffer().to_pybytes()
            #self.keys[key] = 'pyarrow'

        elif method == 'json':
            hashed_value = json.dumps(value)
            self.keys[key] = 'json'

        # handles the posting to the redis
        self.cache_container.hmset(key, {'hashed_value': hashed_value, 'serialization': method})

        if isinstance(timeout, int) is False and timeout is not None:
            raise TypeError('timeout was not a "int", instead recieved {}'.format(type(timeout)))

        if isinstance(timeout, int) is True:
            self.cache_container.expire(key, timeout)

        return None


    #------------------------------------------------------------------------------------
    # GET and POST methods to send and retrive objects from cache
    #------------------------------------------------------------------------------------
    def get(self, key, method):
        """
        Returns object from the redis database if key exists
        """
        if self.cache_container.exists(key) == 1:
            return self._deserialize(key, method=method)
        else:
            raise ValueError("No key {} found in object".format(key))


    def post(self, key, values, serialization, timeout=None):
        """
        Posts key,value to redis where its serilaized by the serialization param
        Parameters:
        -----------
            key=str()
            values=python object
            serialization=str(), of which is 'pickle', 'json', 'pyarrow'
        Returns:
        --------
            python object
        """
        assert isinstance(key, str)
        assert isinstance(serialization, str)
        if timeout is not None:
            assert isinstance(timeout, int), 'timeout parametet needs to be a int'
        assert serialization in ['pickle', 'json', 'pyarrow']

        self._serialize(key, values, serialization, timeout)
    #------------------------------------------------------------------------------------
    # Caching Decorators to be used over functions
    #------------------------------------------------------------------------------------

    def cache(self, force_key=None, method=None, timeout=60 * 180):
        """
        General Decorater function for caching functions, will attempt to cache with the
        correct serialization based on type, else use pickle
        Parameters:
            func = python function, is the input due to wrapping
            method (str, None): Serialization type
            timeout (int, None): whether to timeout a cache, set to one hour by default (60 secs * 60 mins)

        Returns:
            Python Object

        For Decoratior structure look at
        url: https://stackoverflow.com/questions/5929107/decorators-with-parameters
        """
        def _decorator(func):
            @functools.wraps(func)
            def wrapper_df_decorator(*args, **kwargs):
                # Catch for caching
                if self.disable_caching is True:
                    value = func(*args, **kwargs)
                    return value

                # generate key based on called function
                if force_key is None:
                    key = self.key_generator(func, *args, **kwargs)
                else:
                    key = self.key_generator(func, force_key)

                # check if exists in redis
                if self.cache_container.exists(key) == 1:
                    # Pulls data from redis, deserialses and returns the dataframe
                    value = self._deserialize(key)
                else:
                    # Runs the function that was decorated
                    value = func(*args, **kwargs)

                    # Don't pickle none objects
                    if value is None:
                        return value

                    # Handles predicitive assignment
                    if method is None:
                        # if function is dataframe, insert into database
                        if isinstance(value, pd.DataFrame) or isinstance(value, np.ndarray):
                            new_method = 'pickle'
                        # If dict or forced method for json
                        elif isinstance(value, dict):
                            new_method = 'json'
                        # If no assigned forced method or forced method for pickle
                        else:
                            new_method = 'pickle'
                    else:
                        new_method = method

                    # caches the value
                    self._serialize(key, value, method=new_method, timeout=timeout)

                return value
            return wrapper_df_decorator
        return _decorator(force_key) if callable(force_key) else _decorator
